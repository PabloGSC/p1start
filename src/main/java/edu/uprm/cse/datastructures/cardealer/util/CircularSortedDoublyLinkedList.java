package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	//inner Node Class
	private static class Node<E>{
		
		private E element;
		private Node<E> next;
		private Node<E> prev;
		
		public Node(E element, Node<E> next, Node<E> prev) {
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		
		public Node() {
			this.element = null;
			this.next = null;
			this.prev = null;
		}
		
		public E getElement() {
			return element;
		}
		
		public void setElement(E element) {
			this.element = element;
		}
		
		public Node<E> getNext() {
			return next;
		}
		
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		public Node<E> getPrev() {
			return prev;
		}
		
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
		
	}
	
	private Node<E> header;
	private int size;
	private Comparator<E> comparator;
	
	public CircularSortedDoublyLinkedList() {
		this.header = new Node<E>();
		this.size = 0;
		this.comparator = new regularComparator();
	}
	
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.header = new Node<E>();
		this.size = 0;
		this.comparator = comp;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new ListIterator();
	}

	//adds a new obj to the list, returns true when done
	@Override
	public boolean add(E obj) {
		Node<E> nNode = new Node<E>(obj, null, null);
		if(this.isEmpty()){
			this.header.setNext(nNode);
			this.header.setPrev(nNode);
			nNode.setNext(header);
			nNode.setPrev(header);
			this.size++;
		} else{
			Node<E> temp = this.header.getNext();
			while(temp!=this.header && comparator.compare(temp.getElement(), obj) <= 0){
				temp = temp.getNext();
			}
			nNode.setNext(temp);
			nNode.setPrev(temp.getPrev());
			temp.getPrev().setNext(nNode);
			temp.setPrev(nNode);
			this.size++;
		}
		return true;
	}

	@Override
	public int size() {
		return this.size;
	}

	//removes the the first "obj" found on the list, returns true if there was an object to remove
	@Override
	public boolean remove(E obj) {
		if (this.isEmpty()) {
			return false;
		}
		Node<E> temp = header.getNext();
		while(temp.getElement() != null){
			if(temp.getElement()==obj){
				temp.getPrev().setNext(temp.getNext());
				temp.getNext().setPrev(temp.getPrev());
				temp.setNext(null);
				temp.setPrev(null);
				temp.setElement(null);
				this.size--;
				return true;
			}
			else{
				temp = temp.getNext();
			}
		}		
		return false;
	}

	//removes an obj at a given index, returns true if there was an object to remove
	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if (this.isEmpty()) {
			return false;
		}
		this.checkIndex(index);
		if(index == 0){
			Node<E> temp = this.header.getNext();
			temp.getPrev().setNext(temp.getNext());
			temp.getNext().setPrev(temp.getPrev());
			temp.setElement(null);
			temp.setNext(null);
			temp.setPrev(null);
			this.size--;
			return true;
		} else{
			Node<E> temp = findNode(index - 1);
			Node<E> temp2 = temp.getNext();
			temp.setNext(temp2.getNext());
			temp2.getNext().setPrev(temp);
			temp2.setElement(null);
			temp2.setNext(null);
			temp2.setPrev(null);
			this.size--;
			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		if(this.isEmpty()){
			return counter;
		}
		while(this.remove(obj)){
			counter++;
		}
		return counter;
	}

	@Override
	public E first() {
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		this.checkIndex(index);
		Node<E> temp = this.findNode(index);
		return temp.getElement();
	}

	@Override
	public void clear() {
		while(!this.isEmpty()){
			this.remove(0);
		}
	}

	//returns true if the list contains the given parameter 'e'
	@Override
	public boolean contains(E e) {
		Node<E> temp = this.header.getNext();
		for(int i = 0; i<this.size(); i++){
			if(temp.getElement().equals(e))
				return true;
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		if(this.isEmpty()){
			return -1;
		}
		for(int i = 0; i<this.size; i++){
			if(findNode(i).getElement() == e){
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		if(this.isEmpty()){
			return -1;
		}
		for(int i = this.size - 1; i>=0; i--){
			if(findNode(i).getElement() == e){
				return i;
			}
		}
		return -1;
	}

	//added this method to help find a Node at a given index
	private Node<E> findNode(int index) {
		Node<E> temp = this.header.getNext();
		int i = 0;
		while (i < index) {
			temp = temp.getNext();
			i++;
		}
		return temp;
	}
	
	//checks if the given index is out of bounds
	private void checkIndex(int index) {
		if ((index < 0) || (index >= this.size())){
			throw new IndexOutOfBoundsException();
		}
	}
	
	//regular list iterator
	private class ListIterator implements Iterator<E>{

		private Node<E> current;
		
		private ListIterator(){
			this.current = header.getNext();
		}
		
		@Override
		public boolean hasNext() {
			return current.getNext() != header.getNext();
		}

		@Override
		public E next() {
			if(!hasNext()) throw new NoSuchElementException("No more elements");
			E etr = current.getElement();
			current = current.getNext();
			return etr;
		}
	}
	

	private class regularComparator implements Comparator<E>{
		@Override
		public int compare(E o1, E o2) {
			String obj1 = o1.toString();
			String obj2 = o2.toString();
			return obj1.compareTo(obj2);
		}
	}


	public Car[] toArray() {
		int i = 0;
		Car [] res = new Car[this.size];
		Node<E> temp = this.header.getNext();
		while(temp != this.header) {
			res[i] = (Car) temp.getElement();
			i++;
			temp = temp.getNext();
		}
		return res;
	}

}
