package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarService {
  
  private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Car[] getCarList() {
      return cList.toArray();
  }

  @GET
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Car getCar(@PathParam("id") long id){
	  for(Car car : cList){
			if(car.getCarId()==id){
				return car;
			}  
	  }  
	  throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
  }
  
  @POST
  @Path("/add")
  @Produces(MediaType.APPLICATION_JSON)
  public Response addCar(Car car){
      cList.add(car);
      return Response.status(201).build();
  }
  
  @PUT
  @Path("{id}/update")
  @Produces(MediaType.APPLICATION_JSON)
  public Response updateCar(Car car){
	  for(Car c : cList){
		  if(c.getCarId() == car.getCarId()){ 
			  cList.remove(c);
			  cList.add(car);
			  return Response.status(200).build();
		  }
	  }
	  return Response.status(404).build();
  }
  
  @DELETE
  @Path("{id}/delete")
  public Response deleteCar(@PathParam("id") long id){
	  for(Car car: cList) {
		  if(car.getCarId() == id) {
			  cList.remove(car);
			  return Response.status(200).build(); 
		  }	  
	  }
	  return Response.status(404).build();
  }
  
}